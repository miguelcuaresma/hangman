/*Pick a random word
While the word has not been guessed {
Show the player their current progress
Get a guess from the player
If the player wants to quit the game {
Quit the game
}
Else If the guess is not a single letter {
Tell the player to pick a single letter
}
Else {
If the guess is in the word {
Update the player's progress with the guess
}
}
}
Congratulate the player on guessing the word*/

//The array of words to chose from
let words = ["eagle", "doctor", "software", "daisy", "halogen", "leather", "javascript"];

//pick a random word
let chosenWord = words[Math.floor(Math.random() * words.length)];

//
let answerArray = [];
for (let i = 0; i < chosenWord.length; i++) {
	answerArray[i] = "_";
};

let remainingLetters = chosenWord.length;
let allowedGuesses = 7;
while (remainingLetters > 0 && allowedGuesses > 0) {
	alert(answerArray.join(" "));
	let guess = prompt("Guess a letter, or click Cancel to stop playing.")
	if (guess === null) {
	break;
	} else if (guess.length !== 1) {
		alert("Please enter a single letter.")
	} else {
		for (let j = 0; j < chosenWord.length; j++) {
			guess = guess.toLowerCase()
			if (chosenWord[j] === guess && answerArray[j] === "_") {
			answerArray[j] = guess;
			remainingLetters--
			}
		}	
		allowedGuesses--
	}	
};
alert(answerArray.join(" "));
if (answerArray.join("") == chosenWord) {
	alert("Good job! The answer was " + chosenWord);
} else {
	alert("Better luck next time!")
}
